const SERVER = (() => {
  const URL = {
    BASE: 'http://localhost:2100/api/'
  }
  const URL_BASE = ''
  const USERS = 'users'
  return {
    URL_BASE: URL.BASE,
    USERS: `${URL_BASE}${USERS}`
  }
})()

export default SERVER
