import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import VueAxios from 'vue-axios'
import user from './modules/user'
import project from './modules/project'
import createLogger from 'vuex/dist/logger'

import Vuelidate from 'vuelidate'
Vue.use(Vuelidate)
Vue.use(Vuex)
Vue.use(VueAxios, axios)

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
  modules: {
    user,
    project
  },
  strict: debug,
  plugins: debug ? [ createLogger() ] : []
})
