import {
  getAllUsers,
  getUser,
  updateUser,
  createUser,
  deleteUser
} from '../../api/user.api'

const state = {
  user: {
    id: '',
    firstName: '',
    middleName: '',
    lastName: '',
    email: '',
    userName: '',
    description: '',
    gender: '',
    birthdate: null,
    phone: '',
    status: false
  },
  users: [{}]
}

// getters

const getters = {
  getUser (state) {
    return state.user
  },
  getUsers (state) {
    return state.users
  }
}

// actions

const actions = {
  async fetchUsers ({ commit }) {
    try {
      const response = await getAllUsers()
      console.log(response.data.user.rows)
      commit('SET_USERS', response.data.user.rows)
    } catch (error) {
      console.log('Error', error)
      // handle the error here
    }
  },
  async fetchUser ({ commit }, userId) {
    try {
      const response = await getUser(userId)
      console.log(response.data)
      commit('SET_USER', response.data)
    } catch (error) {
      console.log('Error', error)
      // handle the error here
    }
  },
  async updateUser ({ commit }, user) {
    try {
      const response = await updateUser(Object.assign({}, user))
      commit('UPDATE_USER', response.data)
    } catch (error) {
      console.log('Error', error)
      // handle the error here
    }
  },
  async createUser ({ commit }, user) {
    try {
      const response = await createUser(Object.assign({}, user))
      commit('ADD_USER', response.data)
    } catch (error) {
      console.log('Error', error)
      // handle the error here
    }
  },
  async removeUser ({ commit, dispatch }, userId) {
    try {
      await deleteUser(userId)
      await dispatch('fetchUsers')
    } catch (error) {
      console.log('Error', error)
      // handle the error here
    }
  },
  async updateActionUser ({ commit }, payload) {
    try {
      commit('updateUserForm', payload)
    } catch (error) {
      console.log('Error', error)
    }
  }
}

// mutations

const mutations = {
  SET_USERS (state, data) {
    state.users = data
  },
  SET_USER (state, data) {
    state.user = data
  },
  ADD_USER (state, data) {
    state.user = data
  },
  UPDATE_USER (state, data) {
    state.user = data
  },
  REMOVE_USER (state, data) {
    state.users = data
  },
  //
  updateUserForm (state, payload) {
    if (payload.target.cheked !== undefined) {
      state.user[payload.target.id] = payload.target.checked
    } else {
      state.user[payload.target.id] = payload.target.value
    }
  },
  // clear state
  RESET_STATE (state) {
    state.user = {
      id: '',
      firstName: '',
      middleName: '',
      lastName: '',
      email: '',
      userName: '',
      description: '',
      gender: '',
      birthdate: null,
      phone: '',
      status: false
    }
  }
}

export default {
  namespaces: true,
  state,
  getters,
  actions,
  mutations
}
