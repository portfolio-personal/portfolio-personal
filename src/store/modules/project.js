import {
  getAllProjects,
  getProject,
  updateProject,
  createProject,
  deleteProject,
  addUserToProject
} from '../../api/project.api'

const state = {
  project: {
    id: '',
    name: '',
    title: '',
    description: '',
    status: false
  },
  projects: {
    elements: 0,
    page: 0,
    pageSize: 0,
    pages: 0,
    rows: []
  }
}

// getters

const getters = {
  getProject (state) {
    return state.project
  },
  getProjects (state) {
    return state.projects
  }
}

// actions

const actions = {
  async fetchProjects ({ commit }, value) {
    try {
      // console.log('ll0000====> ', value)
      const response = await getAllProjects(value.page, value.pageSize)
      // console.log(response.data)
      commit('SET_PROJECTS', response.data)
    } catch (error) {
      console.log('Error', error)
      // handle the error here
    }
  },
  async fetchProject ({ commit }, projectId) {
    try {
      const response = await getProject(projectId)
      // console.log(response.data)
      commit('SET_PROJECT', response.data)
    } catch (error) {
      // console.log('Error', error)
      // handle the error here
    }
  },
  async updateProject ({ commit }, project) {
    try {
      const response = await updateProject(Object.assign({}, project))
      commit('UPDATE_PROJECT', response.data)
    } catch (error) {
      console.log('Error', error)
      // handle the error here
    }
  },
  async createProject ({ commit }, project) {
    try {
      const response = await createProject(Object.assign({}, project))
      commit('ADD_PROJECT', response.data)
    } catch (error) {
      console.log('Error', error)
      // handle the error here
    }
  },
  async removeProject ({ commit, dispatch }, projectId) {
    try {
      await deleteProject(projectId)
      await dispatch('fetchProjects')
    } catch (error) {
      console.log('Error', error)
      // handle the error here
    }
  },
  async updateActionProject ({ commit }, payload) {
    try {
      commit('updateProjectForm', payload)
    } catch (error) {
      console.log('Error', error)
    }
  },
  async addUserToProjectActions ({ commit, dispatch }, userProject) {
    try {
      await addUserToProject(userProject)
      await dispatch('fetchProject', userProject.projectId)
    } catch (error) {
      console.log('Error', error)
    }
  }
}

// mutations

const mutations = {
  SET_PROJECTS (state, data) {
    state.projects = data
  },
  SET_PROJECT (state, data) {
    state.project = data
  },
  ADD_PROJECT (state, data) {
    state.project = data
  },
  UPDATE_PROJECT (state, data) {
    state.project = data
  },
  REMOVE_PROJECT (state, data) {
    state.projects = data
  },
  //
  updateProjectForm (state, payload) {
    if (payload.target.cheked !== undefined) {
      state.project[payload.target.id] = payload.target.checked
    } else {
      state.project[payload.target.id] = payload.target.value
    }
  },
  // clear state
  RESET_STATE (state) {
    state.project = {
      id: '',
      name: '',
      title: '',
      description: '',
      status: false
    }
  }
}

export default {
  namespaces: true,
  state,
  getters,
  actions,
  mutations
}
