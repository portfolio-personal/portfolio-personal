// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'
import store from './store'
import ProjectList from '@/components/admin/project/ProjectList.vue'
import ModalProjectUser from '@/components/admin/project/ModalProjectUser.vue'
import Default from './layouts/Default.vue'
import NoSidebar from './layouts/NoSidebar.vue'
import Pagination from './components/pagination/Pagination.vue'
import Skill from './components/admin/skill/Skill.vue'
import Category from './components/admin/category/Category.vue'

// Components in global
Vue.component('project-list', ProjectList)
Vue.component('modal-projcet-user', ModalProjectUser)
Vue.component('default-layout', Default)
Vue.component('no-sidebar-layout', NoSidebar)
Vue.component('pagination', Pagination)
Vue.component('skill', Skill)
Vue.component('category', Category)

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  store,
  router,
  components: { App },
  template: '<App/>'
})
