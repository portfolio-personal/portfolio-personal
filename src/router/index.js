import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import User from '@/components/User'
import Login from '@/components/Login'
import EditUser from '@/components/EditUser'
import Project from '@/components/admin/project/Project'
import ProjectAorE from '@/components/admin/project/ProjectAorE'
import Profile from '@/components/admin/profile/Profile'
import Configuration from '@/components/admin/configuration/Configuration'
import Page404 from '@/pages/Page404.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/user',
      name: 'User',
      component: User
    },
    {
      path: '/login',
      name: 'Login',
      meta: { layout: 'no-sidebar' },
      component: Login
    },
    {
      path: '/edit-user/:id',
      name: 'EditUser',
      component: EditUser,
      props: true
    },
    {
      path: '/create-user',
      name: 'CreateUser',
      component: EditUser,
      props: true
    },
    {
      path: '/project',
      name: 'Project',
      component: Project,
      children: [
        {
          path: '/add-project',
          name: 'AddProject',
          component: ProjectAorE
        },
        {
          path: '/edit-project/:id',
          name: 'EditProject',
          component: ProjectAorE
        }
      ]
    },
    {
      path: '/profile',
      name: 'Profile',
      component: Profile
    },
    {
      path: '/configuration',
      name: 'Configuration',
      component: Configuration
    },
    {
      path: '*',
      name: 'Page404*',
      component: Page404
    }
  ]
})
