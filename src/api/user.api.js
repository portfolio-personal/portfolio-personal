import httpClient from './httpClient'
const END_POINT = '/users'

const getAllUsers = () => {
  return httpClient.get(END_POINT)
}

const getUser = (userId) => {
  return httpClient.get(END_POINT + '/' + userId)
}

const createUser = (user) => {
  return httpClient.post(END_POINT, user)
}

const updateUser = (user) => {
  return httpClient.put(END_POINT, user)
}

const deleteUser = (userId) => {
  return httpClient.delete(END_POINT + '/' + userId)
}

export { getAllUsers, getUser, createUser, updateUser, deleteUser }
