import httpClient from './httpClient'
const END_POINT = '/projects'

const getAllProjects = (page, pageSize) => {
  return httpClient.get(END_POINT + '?page=' + page + '&pageSize=' + pageSize)
}

const getProject = (projectId) => {
  return httpClient.get(END_POINT + '/' + projectId)
}

const createProject = (project) => {
  return httpClient.post(END_POINT, project)
}

const updateProject = (project) => {
  return httpClient.put(END_POINT, project)
}

const deleteProject = (projectId) => {
  return httpClient.delete(END_POINT + '/' + projectId)
}

const addUserToProject = (projectUser) => {
  console.log('api --< ', projectUser)
  return httpClient.post('/add-project-user', projectUser)
}

const removeUserToProject = (userId) => {
  return httpClient.post('/remove-project-user' + '/' + userId)
}

export { getAllProjects, getProject, createProject, updateProject, deleteProject, addUserToProject, removeUserToProject }
