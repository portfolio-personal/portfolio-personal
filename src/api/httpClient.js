import axios from 'axios'
// import SERVER from '../config/server.config'

const htttpClient = axios.create({
  baseURL: 'http://localhost:2100/api',
  timeout: 1000,
  headers: {
    'Content-type': 'application/json'
  }
})

export default htttpClient
